
using System.IO;
using UnityEngine;

public class ETCLoader
{
    public FileHeader Header { get; private set; }
    public byte[] Content { get; private set; }
    public byte[] OriginalContent { get; private set; }

    public ETCLoader(string filePath)
    {
        var bytes = File.ReadAllBytes(filePath);
        OriginalContent = bytes;
        Process(bytes);
    }

    public ETCLoader(byte[] bytes)
    {
        OriginalContent = bytes;
        Process(bytes);
    }

    private void Process(byte[] bytes)
    {
        var header = new FileHeader();

        MemoryStream mr = new MemoryStream(bytes);
        BinaryReader br = new BinaryReader(mr);
        MemoryStream mw = new MemoryStream();
        BinaryWriter bw = new BinaryWriter(mw);

        try
        {
            //reading header

            //bypass 12 header length of KTX format
            br.ReadInt32();
            br.ReadInt32();
            br.ReadInt32();
            //endianness - bypass
            br.ReadInt32();

            //glType
            header.glType = br.ReadUInt32();
            //glTypeSize
            header.glTypeSize = br.ReadUInt32();
            //glFormat
            header.glFormat = br.ReadUInt32();
            //glInternalFormat
            header.glInternalFormat = br.ReadUInt32();
            //glBaseInternalFormat
            header.glBaseInternalFormat = br.ReadUInt32();
            //pixelWidth
            header.glPixelWidth = br.ReadUInt32();
            //pixelHeight
            header.glPixelHeight = br.ReadUInt32();
            //pixelDepth
            header.glPixelDepth = br.ReadUInt32();
            //numberOfArrayElements
            header.numberOfArrayElements = br.ReadUInt32();
            //numberOfFaces
            header.numberOfFaces = br.ReadUInt32();
            //numberOfMipmapLevels
            header.numberOfMipmapLevels = br.ReadUInt32();
            //bytesOfKeyValueData
            header.kvData = br.ReadUInt32();

            Header = header;

            for (int i = 0; i < header.numberOfMipmapLevels; i++)
            {
                uint len = br.ReadUInt32();
                bw.Write(br.ReadBytes((int)len));
            }

            Content = mw.ToArray();
        }
        finally
        {
            //bw.Dispose();
            //br.Dispose();
            mw.Dispose();
            mr.Dispose();
        }
    }

    public struct FileHeader
    {
        public uint glType;
        public uint glTypeSize;
        public uint glFormat;
        public uint glInternalFormat;
        public uint glBaseInternalFormat;
        public uint glPixelWidth;
        public uint glPixelHeight;
        public uint glPixelDepth;
        public uint numberOfArrayElements;
        public uint numberOfFaces;
        public uint numberOfMipmapLevels;
        public uint kvData;
    }

    public Texture2D ToTexture2D()
    {
        Texture2D txt = new Texture2D((int)Header.glPixelWidth, (int)Header.glPixelHeight, TextureFormat.ETC_RGB4, Header.numberOfMipmapLevels > 1, false);
        byte[] dataArr = ToDataArray();

        txt.LoadRawTextureData(dataArr);
        txt.Apply();

        return txt;
    }

    public byte[] ToDataArray()
    {
        return Content;
    }
}