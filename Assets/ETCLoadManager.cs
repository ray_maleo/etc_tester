using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ETCLoadManager : MonoBehaviour
{
    public TMP_Text gMemSizeText;

    public Renderer pngRenderer;
    public Renderer etcRenderer;

    public TextAsset rays_png;
    public TextAsset rays_etc_base;
    public TextAsset rays_etc_alpha;

    // Start is called before the first frame update
    void Start()
    {
        rays_png = (TextAsset)Resources.Load("Rays_PNG", typeof(TextAsset));
        rays_etc_base = (TextAsset)Resources.Load("Rays_ETC_Base", typeof(TextAsset));
        rays_etc_alpha = (TextAsset)Resources.Load("Rays_ETC_Alpha", typeof(TextAsset));

    }

    public void OnPNGButtonClick()
    {
        Texture2D tex = new Texture2D(1, 1);
        tex.LoadImage(rays_png.bytes);

        pngRenderer.material.SetTexture("_MainTex", tex);
    }

    public void OnETCButtonClick()
    {
        ETCLoader loader = new ETCLoader(rays_etc_base.bytes);
        Texture2D tex = loader.ToTexture2D();
        etcRenderer.material.SetTexture("_MainTex", tex);

        loader = new ETCLoader(rays_etc_alpha.bytes);
        tex = loader.ToTexture2D();
        etcRenderer.material.SetTexture("_AlphaTex", tex);
    }

    void Update()
    {
        PrintInfo();

        CheckInput();
    }

    private void PrintInfo()
    {
        gMemSizeText.text = string.Format("vram: {0} Mb", SystemInfo.graphicsMemorySize.ToString());
    }

    private void CheckInput()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }
}
